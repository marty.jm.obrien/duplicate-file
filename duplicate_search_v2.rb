require 'YAML'

filename = Dir['**/*.{JPG,jpg}']

basename_arr = []
check_arr = []
dup_arr = []

filename.each do |f|
  without_path = File.basename(f)
  basename_arr.push(without_path)
end

dup_arr.push(basename_arr.find_all { |f| basename_arr.count(f) > 1})

duplicates_file = "duplicates.txt"

duplicate_output = dup_arr.to_yaml

File.open duplicates_file, 'w' do |f|
  f.write duplicate_output
end
