# require 'YAML'

puts
puts "Searching for duplicate files"
puts

# Search directory for files ending in .JPG or .jpg and assign them to filename
filename = Dir['**/*.{JPG, jpg}']

# Create arrays for storing
basename_arr = []
dup_arr = []

# Loop through all the files found and remove their path
filename.each do |f|
  without_path = File.basename(f)
  basename_arr.push(without_path)
end

# Find all files names which have more than one instance and store them
dup_arr.push(basename_arr.find_all { |f| basename_arr.count(f) > 1})

pic_count = 1

# Loop through the stored duplicate names and check each one against the Original
# search. For every match add pic_count to the end
dup_arr.each do |f|
  filename(File.basename(f)).each do |x|
    if x == f && f[-1] != pic_count
      File.rename x, x + pic_count
    end
  end
  pic_count = pic_count + 1
end



# duplicates_file = "duplicates.txt"
#
# duplicate_output = dup_arr.to_yaml
#
# File.open duplicates_file, 'w' do |f|
#   f.write duplicate_output
# end
