puts
puts "Searching drive for duplicates."

filename = Dir['**/*.{JPG, jpg}']

basename_arr = []
dup_num = 1

filename.each do |f|
  old_name = File.basename(f)
  #new_name = f.insert(-5, dup_num.to_s)
  if basename_arr.include?(old_name)
    #File.rename(f, new_name)
    File.rename(f, f + dup_num.to_s)
    dup_num = dup_num + 1
  end

  basename_arr.push(old_name)

end

puts
puts "Search complete. Duplicates renamed successfully."
puts
