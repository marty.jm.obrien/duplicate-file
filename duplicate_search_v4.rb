puts "Searching drive for duplicates."

filename = Dir['**/*.{JPG, jpg}']

basename_hash = Hash.new(0)

filename.each do |f|
  print "."
  container = File.extname(f)
  old_name = File.basename(f, File.extname(f))

    if basename_hash.has_key?(old_name)
      new_file = old_name + basename_hash[old_name].to_s + container
      a = File.rename(f, new_file)
    else
      basename_hash[old_name] = 0
    end

  basename_hash[old_name] += 1

end

puts
puts "Search complete. Duplicates renamed successfully."
puts
