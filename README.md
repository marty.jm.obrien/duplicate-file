Duplicate File Search - Version 4 - Created by Marty O'Brien
Last updated 01/02/2017

Overview:

Duplicate file searching app which searches a directory for files ending in a specified extention and adds an additional incrementing number
to the end of the file if a duplicate is found.


UPDATE

- 01/02/2017 - Version 4 - I finally got back to this and fixed the naming issue. I found two main issues with the code. The first was that whilst 
  I removed the file path with File.basename I wasn't adding in an argument to remove the extention as well. By putting the ext into a variable before
  removing it from the file name I was able to have the duplicate count number added to the end of the file and then put the original ext back on.
  The next issue I had was keeping track of the file names so that the duplicate count could increment individually to each file. 
  Using the array I could only have it continually count up regardless of how many times a single file was duplicated. I fixed this by inplementing
  a hash. With the addition of a value I can increment against a file name the duplicate counts are specic.

Current problems and on-going issues:

- *Fixed* The incrementing number is added after the container name, not the filename. I need to find a way of getting the number before the container. 
  I have attempted to remove the container and then add it back and also count four characters back and insert the number but each time I am hit 
  by an error stating that the filename cannot be found. This is where it got a bit confusing as it's dealing with objects outside of the code itself.
- *Fixed* Version 1 - Initial attempt in which a file was created listing all the names which occur more than once. 
- *Fixed* Version 2 - First attempt at actually finding and renaming in one go.
- *Partially Fixed* Version 3 - Have to copy the file onto your drive of choice and run it. Cannot currently increment duplicate numbers for individual files.
  Renaming occurs at the end of the file (IMG808.jpg1).
- Need to find a way to have duplicates go to a specific folder or copy into their original folder. Current default is the master directory.

Project progression:

- Build into a user interface form which allows the user to specify where they want to search instead of having 
  to drag around the file itself. 
- Create an option to select which file extentions you would like to search.